import Vue from 'vue';
import '@/plugins/bootstrap-vue';
import Vuex from 'vuex';

import AppDate from '@/components/AppDate.vue';

Vue.component('AppDate', AppDate);

Vue.use(Vuex);
