const path = require('path');

module.exports = {
  stories: ['../src/stories/**/*.stories.[tj]s'],
  webpackFinal: async (config, { configType }) => {
    config.resolve.alias = {
      ...config.resolve.alias,
      '@': path.resolve(__dirname, '../src/'),
    }
    return config;
  },
  addons: [
    {
      name: '@storybook/preset-scss',
      options: {
        sassLoaderOptions: {
          prependData: `@import "@/plugins/_theming.scss";`
        }
      }
    },
    '@storybook/preset-typescript',
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-knobs',
  ]
};
