module.exports = {
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "@/plugins/_theming.scss";`
      }
    }
  },
  configureWebpack: {
    optimization: {
      splitChunks: {
        chunks: "all"
      }
    }
  }
};
