import { storiesOf } from "@storybook/vue";
import NavBar from "@/components/NavBar.vue";

storiesOf("NavBar", module).add("default", () => ({
  components: { NavBar },
  template: "<NavBar />"
}));
