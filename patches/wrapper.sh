# taken from https://stackoverflow.com/a/39038298
# If we could reverse the patch, then it has already been applied; skip it
if patch --dry-run --reverse --force -p1 < patches/@storybook+vue+5.3.14.patch >/dev/null 2>&1; then
  echo "Patch already applied - skipping."
else # patch not yet applied
  echo "Patching..."
  patch -Ns -p1 < patches/@storybook+vue+5.3.14.patch || echo "Patch failed" >&2 && exit 1
fi
