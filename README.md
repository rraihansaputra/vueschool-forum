# vueschool-forum
This is a test repository to get Vue, TypeScript, Storybook, and BootstrapVue to work reliably and predictably in a project.

[![pipeline status](https://gitlab.com/rraihansaputra/vueschool-forum/badges/master/pipeline.svg)](https://gitlab.com/rraihansaputra/vueschool-forum/-/commits/master)

Live Site delpoyed from CI: https://vueschool-forum.netlify.app
Live Storybook site deployed from CI: https://vueschool-forum-storybook.netlify.app

## Steps to Here
1. `vue create` with these presets:
```
Vue ClI v4.2.3
1. Features:
    - Babel
    - TypeScript
    - PWA Support
    - Router
    - Vuex
    - CSS Pre-processors
    - Linter / Formatter
    - Unit Testing
2. Use class-style component syntax? no
3. Use Babel alongside TypeScript? Y
4. Use history mode for router? Y
5. Pick a CSS pre-processor: node-sass
6. Pick a linter / formatter config: ESLint + Prettier
7. Pick additional lint features:
    - Lint on save
    - Lint and fix on commit
8. Unit testing: Jest
9. Where do you prefer placing config?: In dedicated config files
```
2. Install VueBootstrap through `vue add bootstrap-vue`
3. Change the `plugins/bootstrap-vue.js` by adding `plugins/_theming.scss` and `plugins/custom.scss`
4. Add storybook through `npx -p @storybook/cli sb init --type vue`
5. Apply the patch using `patch -p1 < patches/@storybook+vue+5.3.14.patch`
6. Move `./stories` to `src/stories` and change `.storybook/main.js` to search under `src/`
7. `yarn add --dev @storybook/preset-scss @storybook/preset-typescript`
8. Change the initial stories to from `*.stories.js` to `*.stories.ts` and change `.storybook/main.js` to include .ts by changing regex from `stories.js` to `stories.[tj]s`.
9. Set up Storybook Webpack to load Vue SCSS by configuring the scss preset through `.storybook/main.js`, and add prepend css to include theming for the app
10. configure `.storybook/preview.js` to load Vue plugins as the app, in this case the `plugins/bootstrap-vue.js` and base Vuex
11. set up gitlab ci/cd to build and deploy to netlify

..Thats involved. I need to create a template, honestly.

## Tooling
Use VSCode with Vetur for all the Vue interpolation feature. WIP suitable template to start components.

## Docker
Currently all files under `dockerfiles/` are meant to be used to load the built `dist/` directory to a suitable nginx docker image for deployment. It is not set up to be a development image.
To optimize the build process, the `Dockerfile.prod.dockerignore` explicitly only forwards the `dist/` and `dockerfiles/` folder for the build context.
The build command is as follows:
```
DOCKER_BUILDKIT=1 docker build -t test-dockerize -f dockerfiles/Dockerfile.prod .
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
