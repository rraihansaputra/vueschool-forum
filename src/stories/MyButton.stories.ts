import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";

import { storiesOf } from "@storybook/vue";

import MyButton from "@/components/MyButton.vue";

// The _standard_ way of writing Stories
//
// export default {
//   title: "Button",
//   component: MyButton
// };
//
// export const Text = () => ({
//   components: { MyButton },
//   template: '<my-button @click="action">Hello Button</my-button>',
//   methods: { action: action("clicked") }
// });
//
// export const Emoji = () => ({
//   components: { MyButton },
//   template: '<my-button @click="action">😀 😎 👍 💯</my-button>',
//   methods: { action: action("clicked") }
// });

const paddedButton = () => {
  return {
    template: '<div style="padding: 3rem;"><story/></div>'
  };
};

// Using storiesOf and .add to add suggestion and enforce correct objects
storiesOf("Button", module)
  // add decorators this way
  .addDecorator(paddedButton)
  .add("with text", () => ({
    components: { MyButton },
    template: `<my-button @click="action">Test Button</my-button>`,
    methods: { action: action("clicked") }
  }))
  .add("with emoji", () => ({
    components: { MyButton },
    template: `<my-button @click="action">😀 😎 👍 💯</my-button>`,
    methods: { action: action("clicked") }
  }));
