import { storiesOf } from "@storybook/vue";

import ThreadListItem from "@/components/ThreadListItem.vue";

const defaultThreadData = {
  ".key": "1",
  title: "Test Thread",
  publishedAt: 382938716,
  posts: [1, 2, 3]
};

const defaultPoster = "John Doe";

const defaultProps = {
  thread: { default: () => ({ ...defaultThreadData }) },
  poster: { default: () => defaultPoster }
};

storiesOf("ThreadListItem", module)
  .add("default", () => ({
    components: { ThreadListItem },
    template: `<ThreadListItem :thread="thread" :poster="poster" />`,
    props: { ...defaultProps }
  }))
  .add("0 replies", () => ({
    components: { ThreadListItem },
    template: `<ThreadListItem :thread="thread" :poster="poster" />`,
    props: {
      ...defaultProps,
      thread: { default: () => ({ ...defaultThreadData, posts: [1] }) }
    }
  }));
